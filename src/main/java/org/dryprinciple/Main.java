package org.dryprinciple;

public class Main {
    public static void main(String[] args) {

        Dog dog = new Dog();
        dog.woof();
        dog.eatFood();

        Cat cat = new Cat();
        cat.meow();
        cat.eatFood();
    }
}