# DRY Principle Story.
**To read :** [https://gitlab.com/shivshanker9498/coding-principle-dry-java] 

**Estimated reading time** : 15 minutes.

## Story outline :

This story is about one of the coding principles
i.e. DRY (Don't Repeat Yourself Principle)
This story will demonstrate how we can avoid repetition of  
piece of code and maintain our code with standard
coding principles and practices followed in industry.
which really help programmer to read, maintain and understand 
the flow of source code.

## Story Organization

**Story branch :** master
> git checkout master

Tags : #clean_code, #DRY_principle